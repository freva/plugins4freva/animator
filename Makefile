export PATH := $(PWD)/plugin_env/bin:$(PATH)
.PHONY: docs conda cartopy

all: conda build

build:
	# Adjust this step to build additional plugin dependencies
	echo "This step should be adjusted according to the build procedure"

conda:
	# This installs a conda environment
	rm -rf ./plugin_env
	mamba env create --prefix ./plugin_env -f plugin-env.yml --force

pip:
	# Use this if the plugin is packaged via setuptools
	./plugin_env/bin/python -m pip install .

cartopy:
	# Use this if cartopy was installed and additional maps are downloaded.
	$(eval DATA_DIR =  $(shell plugin_env/bin/python -c 'from cartopy import config;print(config["repo_data_dir"])'))
	./plugin_env/bin/cartopy_feature_download.py gshhs physical cultural \
		cultural-extra -o $(DATA_DIR) --no-warn --ignore-repo-data

docs:
	# Use this create plugin docs
	make -C docs clean
	make -C docs html
