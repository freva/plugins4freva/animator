import argparse
from contextlib import contextmanager
from datetime import datetime
from functools import partial
import json
from multiprocessing import cpu_count
from pathlib import Path
import re
import shlex
from subprocess import run, PIPE
import sys
import shutil
from tempfile import TemporaryDirectory
import warnings

import humanize
import xarray as xr
import cf_xarray.units
import dask.bag
import pint_xarray
from pint_xarray import unit_registry as ureg
import pandas as pd
import pint
import numpy as np

from mapfactory.MapFactory import plot_map, get_filenames, _pool_wrapper, Plotter


class UnitConverter:

    UndefinedUnitError = pint.UndefinedUnitError
    DimensionalityError = pint.DimensionalityError

    units = pint.UnitRegistry(
        autoconvert_offset_to_baseunit=True, force_ndarray_like=True
    )

    # Capture v0.10 NEP 18 warning on first creation
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        units.Quantity([])

    # For pint 0.6, this is the best way to define a dimensionless unit. See pint #185
    units.define("percent = 1e-2 frac = pct")
    units.define("@alias percent = %")
    units.define("fraction = [] = frac")
    units.define("@alias fraction = frac = frac. = fract. = fract = 1 = 1.")
    # Define commonly encountered units not defined by pint
    units.define(
        "degrees_north = degree = degrees_N = degreesN = degree_north = degree_N "
        "= degreeN"
    )
    units.define(
        "degrees_east = degree = degrees_E = degreesE = degree_east = degree_E "
        "= degreeE"
    )
    units.define("@alias h = hour")
    # Alias geopotential meters (gpm) to just meters
    units.define("@alias meter = metre = gpm")

    # Silence UnitStrippedWarning
    if hasattr(pint, "UnitStrippedWarning"):
        warnings.simplefilter("ignore", category=pint.UnitStrippedWarning)

    @classmethod
    def convert(cls, dset, outputunit):
        units = cls.units
        if not outputunit:
            return dset
        try:
            inputunit = dset.attrs["units"]
        except KeyError:
            warnings.warn("No input unit given, cannot convet")
            return dset
        if inputunit.lower() == outputunit.lower():
            return dset
        unit = [inputunit, outputunit]
        dt = pd.Timedelta(dset.time.diff(dim="time").values[0]).total_seconds()
        # Try to convert the units to pint.units objects
        for n, uu in enumerate(unit):
            if "frac" in unit[n] or unit[n] in ("1", "1."):
                # Non-units assignment
                uu = ""
            elif "%" in unit[n]:
                uu = "percent"
            else:
                # Pint cannot deal with units like s-1
                # try to identify those units and replace numeric values
                # with a '^' (if numeric characters are present but no
                # power sign is given - like in m s-1 -> m s^-1)
                for i in re.findall("[-\d]+", unit[n]):
                    if "^" not in unit[n] or "**" in unit[n]:
                        uu = uu.replace(i, f"^{i}")
            try:
                unit[n] = units(uu.replace("-^", "^-").replace("--", "-"))
            except Exception as e:
                print(
                    f"Unit conversion failed, input unit {inputunit}, "
                    f"target unit {outputunit}, unit passed to pint: {unit[n]}"
                )
                raise e
        inunit, outunit = unit
        quant = dset.pint.quantify()
        outputunit = str(units(outputunit).units)

        def _is_length_dim(inunit):
            return [dim[1:-1] for dim in inunit.units.dimensionality] == ["length"]

        def tuple_pos(mul):
            for (nn, k) in enumerate(mul.dimensionality.keys()):
                if k == "[time]":
                    return nn

        if inunit == outunit:
            # If the units are euqal we do not need to do anything
            dset.attrs["units"] = outputunit
            return dset
        rho_w = 1000 * units("kg/m^3")  # ~ Density of water
        density_unit = units("kg/m**2").units
        if _is_length_dim(inunit) and outunit.units == density_unit:
            # Most likely this is a water based variable, hence convert
            # a water hight unit to a density unit using the density of water
            dset.attrs["units"] = outputunit
            return dset * (inunit.to_base_units() * rho_w).magnitude
        if _is_length_dim(outunit) and inunit.units == density_unit:
            # Same as above but vice versa
            conv = outunit.to_base_units() / rho_w
            dset.attrs["units"] = outputunit
            return dset * conv.to(inunit).magnitude
        mass_flux_unit = units("kg/m**2/s").units
        flux = (rho_w / (dt * units("s"))).to_base_units()
        speed_unit = units("meter/second").units
        if _is_length_dim(inunit) and outunit.units == mass_flux_unit:
            # Most likely this is a water based variable that needs to be
            # converted to flux unit by aplying rho_w and the time difference
            dset.attrs["units"] = outputunit
            return dset * (flux * inunit).to_base_units().magnitude
        if _is_length_dim(outunit) and inunit.units == mass_flux_unit:
            # Same flux case as above, just vice versa
            dset.attrs["units"] = outputunit
            return dset * ((outunit.to_base_units() / flux).to(inunit)).magnitude
        # Do a 'normal' magnitude conversion
        if inunit.units == density_unit and outunit.units == mass_flux_unit:
            # Input is density output mass flux -> divide by time unit
            dset.attrs["units"] = outputunit
            return dset * 1 / (dt * units("s")).to_base_units().magnitude
        if inunit.units == mass_flux_unit and outunit.units == density_unit:
            # Same as above but vice versa
            dset.attrs["units"] = outputunit
            return dset * (dt * units("s")).to_base_units().magnitude
        if (
            inunit.to_base_units().units == speed_unit
            and outunit.units == mass_flux_unit
        ):
            inunit = (inunit * rho_w).to_base_units()
        if (
            inunit.units == mass_flux_unit
            and outunit.to_base_units().units == speed_unit
        ):
            timeunit = units(outunit.to_tuple()[-1][tuple_pos(outunit)][0])
            outunit = (outunit * rho_w).to_base_units() * units("s") / timeunit
        conv = quant.pint.to(outunit.units).pint.dequantify()
        conv.attrs["units"] = outputunit
        return conv


@contextmanager
def hide_output():
    savestderr = sys.stderr

    class Devnull(object):
        def write(self, _):
            return

        def flush(self):
            pass

    sys.stderr = Devnull()
    try:
        yield
    finally:
        sys.stderr = savestderr


def parse_config(argv=None):
    """Construct command line argument parser."""

    argp = argparse.ArgumentParser
    ap = argp(
        prog="map_plotter",
        description="""Plot some maps of different grids""",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    ap.add_argument(
        "configfile", metavar="configfile", type=Path, help="The configuration file."
    )
    ap.add_argument(
        "--start",
        default=None,
        type=pd.Timestamp,
        help="First timestep to be considered, None to take end from data (default)",
    )
    ap.add_argument(
        "--end",
        default=None,
        type=pd.Timestamp,
        help="The last timestep to be consideered, None to take end from data "
        "(default)",
    )
    ap.add_argument(
        "--vmax",
        default=None,
        type=float,
        help="Max for display range, None to calculate from data.",
    )
    ap.add_argument(
        "--vmin",
        type=float,
        default=None,
        help="Min for display range, Non to calculate from data.",
    )
    ap.add_argument(
        "--cmap", default="RdYlBu_r", type=str, help="Colormap used for plotting"
    )
    ap.add_argument(
        "--projection",
        default="Mollweide",
        type=str,
        help="Map projection to be used as cartoy call",
    )
    ap.add_argument(
        "--proj_centre", default=50, type=int, help="Centre of the map projection"
    )
    ap.add_argument(
        "--pic_size",
        default=[1360, 900],
        type=int,
        help="Image size in pixels",
        nargs=2,
    )
    ap.add_argument("--plot_title", default="", type=str, help="The title of the Plot"),
    ap.add_argument("--cbar_label", default="", type=str, help="The colorbar label")
    ap.add_argument(
        "--time_mean",
        default=None,
        type=str,
        help="Interval for resampling the time axis, None for no resampling",
    )
    ap.add_argument(
        "--time_method",
        default="mean",
        type=str,
        choices=("mean", "max", "min", "sum"),
        help="The method that is applied when resampling the time axis",
    )
    ap.add_argument(
        "--output_unit",
        default=None,
        type=str,
        help="The target output unit for unit conversion",
    )
    ap.add_argument(
        "--lonlatbox",
        default=None,
        type=float,
        nargs=4,
        help="Extend of a rectangular lonlat box",
    )
    ap.add_argument(
        "--level",
        default=None,
        type=float,
        help="The vertical level that is to be slected",
    )
    ap.add_argument(
        "--linecolor",
        default="k",
        type=str,
        help="Colors of the coast lines in the map",
    )
    ap.add_argument(
        "--suffix",
        default="gif",
        type=str,
        help="Filetype of the animation",
        choices=("gif", "mp4"),
    )
    ap.add_argument(
        "--fps", default=10, type=int, help="Frames per second of the animation."
    )
    args = ap.parse_args()
    cfg_file = args.configfile.expanduser().absolute()
    with cfg_file.open() as f:
        config = json.load(f)
    for k, v in args._get_kwargs():
        if k != "configfile":
            config.setdefault(k, v)
    config["projection"] = "{}({})".format(
        config["projection"], config.pop("proj_centre")
    )
    config["config_file"] = str(cfg_file)
    return config


def animate(lon, lat, dset, outputfile, plot_range, *args, **kwargs):
    """Create an animation of a xarray.DtaArray.

    Parameters:
    ===========
        lon (xr.DataArray, np.array):
                the Longitude Vector
        lat (xr.DataArray, np.array):
                the Latitude Vector
        dset (xr.DataArray):
                The data to be plotted
        outfile (str, pathlib.Path):
            The filename of the animation
        plot_range (xr.DataArray):
                xarray DataArray with the timesteps to be considered
    """
    fps = kwargs.pop("fps", 10)
    # dset = dset.load()
    try:
        pargs = (dset.sel(time=i) for i in plot_range)
        tmp_data = dset.isel(time=0).load()
    except TypeError:
        pargs = (dset.load(),)
        tmp_data = pargs
        outputfile = str(Path(outputfile).with_suffix(".png"))

    args = (lon, lat) + args
    with TemporaryDirectory() as td:
        kwargs["dirname"] = td
        plotter = partial(_pool_wrapper, func=plot_map, args=args, kwargs=kwargs)
        try:
            plotter(tmp_data)
        except Exception:
            pass
    with TemporaryDirectory(prefix="map_plotter_") as td:
        kwargs["dirname"] = td
        plotter = partial(_pool_wrapper, func=plot_map, args=args, kwargs=kwargs)
        dask_bag = dask.bag.from_sequence(pargs, npartitions=min(cpu_count(), 60))
        _ = dask_bag.map(plotter).compute(scheduler="processes")
        return _create_animation(td, outputfile, fps=fps)


def _create_animation(outdir, outputfile, fps=10):
    """FFmpeg calls."""
    # TODO: this is awkward
    ffmpeg = "ffmpeg"
    outputfile = Path(outputfile)
    outputfile.parent.mkdir(exist_ok=True, parents=True)
    if outputfile.suffix in (".png"):
        for file in Path(outdir).rglob("*.png"):
            shutil.copy(file, outputfile)
        return outputfile
    with TemporaryDirectory(prefix="animate") as td:
        outfile = Path(td) / "out.mp4"
        if outputfile.suffix == ".mp4":
            outfile = outputfile
        cmd = shlex.split(
            f"{ffmpeg} -framerate {fps} -pattern_type glob -i '{outdir}/*.png' "
            "-c:v libx264 -pix_fmt yuv420p -vf 'scale=trunc(iw/2)*2:trunc(ih/2)*2' "
            f"-y {outfile}"
        )
        run(cmd, stdout=PIPE, stderr=PIPE, check=True)
        if outputfile.suffix == ".mp4":
            return outputfile
        outputfile = outputfile.with_suffix(".gif")
        cmd = shlex.split(
            f"{ffmpeg} -y -i {outfile} -vf palettegen {Path(td)/'palette.png'}"
        )
        run(cmd, stdout=PIPE, stderr=PIPE, check=True)
        cmd = shlex.split(
            f"{ffmpeg} -y -i {outfile} -i {Path(td)/'palette.png'} "
            f"-filter_complex paletteuse -r 10 {outputfile}"
        )
        run(cmd, stdout=PIPE, stderr=PIPE, check=True)
    return outputfile


def main(cfg):

    config_file = cfg.pop("config_file")
    start_time = datetime.now()
    fps = cfg.pop("fps", 10)
    print("Setp 1: Collecting all files", flush=True)
    new_files = []
    files = []
    for f in cfg["files"]:
        f = Path(f)
        if f.is_file():
            new_files.append(f)
        else:
            if f.is_dir():
                d = f
                suffix = "*.nc"
            else:
                d = f.parent
                suffix = f.name
            new_files += [str(ff) for ff in d.rglob(suffix)]
    for f in new_files:
        fn = get_filenames(str(f), cfg["start"], cfg["end"])
        if fn:
            files.append(fn)
    files = files or new_files
    print("Step 2: Opening the netcdf-files, collecting metadata", flush=True)
    mf_kwargs = dict(
        parallel=False,
        combine="by_coords",
        use_cftime=True,
    )
    with hide_output():
        dset = xr.open_mfdataset(sorted(files), **mf_kwargs)
    for key in ("start", "end"):
        if cfg[key]:
            cfg[key] = pd.Timestamp(cfg[key])
    start = cfg["start"] or min(dset.time.values)
    end = cfg["end"] or max(dset.time.values)
    try:
        dset = dset.sel(time=slice(start, end))
    except TypeError:
        warnings.warn("Cloud select time slices %s %s", start, end)
    try:
        arg = dset["rotated_pole"].attrs
        mapping_method = "from_rotated_grid"
    except KeyError:
        arg = cfg["projection"] or "Mollweide(50)"
        mapping_method = "from_global_grid"
    data = dset[cfg["variable"]]
    # TODO: This only works for rectangular grids
    lonname = data.dims[-1]
    latname = data.dims[-2]
    start_s = Plotter.get_timestamp(dset.isel(time=0), fmt="%Y%m%dT%H%M")
    end_s = Plotter.get_timestamp(dset.isel(time=-1), fmt="%Y%m%dT%H%M")
    start_s = start_s or "fx"
    end_s = end_s or "fx"
    attrs = data.attrs
    if "lev" in data.dims:
        if cfg["level"] is None or cfg["level"] == 0:
            data = data.isel(lev=0)
        else:
            lev = np.argmin(np.fabs(data.coords["lev"].values - cfg["level"]))
            data = data.isel(lev=lev)
    print("Step 3: Converting Units", flush=True)
    data = UnitConverter.convert(data, cfg["output_unit"])
    attrs["units"] = data.attrs.get("units", "")
    if cfg["time_mean"]:
        if cfg["time_mean"].lower() in ("all", "total"):
            data = getattr(data, cfg["time_method"])(dim="time")
            data["time"] = None
        else:
            data = getattr(data.resample(time=cfg["time_mean"]), cfg["time_method"])()
        data.attrs = attrs
    print("Step 4: Loading the dataset, getting min/max values", flush=True)
    if data.time.values.size < 2:
        tmp_data = data.load()
    else:
        # Load only the first 5 timesteps
        tmp_data = data.isel(time=slice(0, 5))
    vmin, vmax = cfg["vmin"], cfg["vmax"]
    if vmin is None or vmax is None:
        tmp_data = tmp_data.load()
        quant = tmp_data.quantile([0.05, 0.95]).values
        del tmp_data
        if vmin is None:
            vmin = quant[0]
        if vmax is None:
            vmax = quant[-1]
    cbar_label = (
        cfg["cbar_label"]
        or f"{attrs.get('long_name', cfg['variable'])} [{attrs.get('units','')}]"
    )
    title = cfg["plot_title"] or " "
    fig_size = tuple(np.array(cfg["pic_size"]) / 150)  # pixel to inch
    print("Step 5: Creating the animation", flush=True)
    time_str = start_s + "-" + end_s
    outfile = Path(cfg["outdir"]) / f'{cfg["variable"]}_{time_str}.{cfg["suffix"]}'
    outfile = animate(
        data[lonname].values,
        data[latname].values,
        data,
        outfile,
        data.time,
        arg,
        cbar_label=cbar_label,
        title=title,
        mapping_method=mapping_method,
        color=cfg["linecolor"],
        vmax=float(vmax),
        vmin=float(vmin),
        figsize=fig_size,
        cmap=cfg["cmap"],
        fps=fps,
        lonlatbox=cfg.get("lonlatbox"),
    )
    within = humanize.naturaldelta(datetime.now() - start_time)
    print(f"Created animation {outfile} in {within}", flush=True)
    with open(config_file, "w") as f:
        json.dump({"output_file": str(outfile)}, f)


if __name__ == "__main__":

    cfg = main(parse_config(sys.argv))
