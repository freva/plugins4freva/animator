import json

try:
    from pathlib2 import Path
except ImportError:
    from pathlib import Path
from tempfile import NamedTemporaryFile

from evaluation_system.api import plugin, parameters
from evaluation_system.model.file import DRSFile


class Animator(plugin.PluginAbstract):

    __category__ = "visualisation"
    __tags__ = ["plotting"]
    tool_developer = {"name": "Martin Bergemann", "email": "bergemann@dkrz.de"}
    __short_description__ = "Animate data on lon/lat grids"
    __long_description__ = (
        "Create animations (in gif or mp4 format) "
        "This tool creates plots of solr facets and "
        "an animation."
    )
    __version__ = (2022, 7, 15)
    __parameters__ = parameters.ParameterDictionary(
        parameters.File(
            name="input_file",
            item_separator=",",
            help="NetCDF input file(s), you can choose multiple files"
            " separated by a , or use a global pattern for multiple files"
            " chose this option only if you don't want Freva to find "
            "files by seach facets",
            default=None,
        ),
        parameters.SolrField(
            name="variable",
            mandatory=True,
            facet="variable",
            help="Variable name (only applicable if you didn't choose an input file)",
        ),
        parameters.SolrField(
            name="project",
            default=None,
            facet="project",
            help="Project name (only applicable if you didn't choose an input file)",
        ),
        parameters.SolrField(
            name="product",
            default=None,
            facet="product",
            help="Product name (only applicable if you didn't choose an input file)",
        ),
        parameters.SolrField(
            name="experiment",
            default=None,
            facet="experiment",
            help="Experiment name (only applicable if you didn't choose an input file)",
        ),
        parameters.SolrField(
            name="institute",
            default=None,
            facet="institute",
            help="Institute name (only applicable if you didn't choose an input file)",
        ),
        parameters.SolrField(
            name="model",
            default=None,
            facet="model",
            help="Model name (only applicable if you didn't choose an input file)",
        ),
        parameters.SolrField(
            name="time_frequency",
            default=None,
            facet="time_frequency",
            help="Time frequency name (only applicable if you didn't choose an input file)",
        ),
        parameters.SolrField(
            name="ensemble",
            default=None,
            facet="ensemble",
            help="Ensemble name (only applicable if you didn't choose an input file)",
        ),
        parameters.String(
            name="start",
            default=None,
            help="Define the first time step to be plotted, leave blank if "
            "taken from data",
        ),
        parameters.String(
            name="end",
            default=None,
            help="Define the last time step to be plotted, leave blank if "
            "taken from data",
        ),
        parameters.String(
            name="time_mean",
            default=None,
            help="Select a time interval if time averaging/min/max/sum "
            "should be applied along the time axis. This can be D for "
            "daily, M for monthly 6H for 6 hours etc. Leave blank if the "
            "time axis should not be resampled (default) or set to "
            "<em>all</em> if you want to collapse the time axis.",
        ),
        parameters.SelectField(
            name="time_method",
            default="mean",
            options={"mean": "mean", "max": "max", "min": "min", "sum": "sum"},
            help="If resampling of the time axis is chosen (default: no) "
            "set the method: mean, max or min. <b>Note:</b> This has "
            "only an effect if the above parameter for <em>time_mean</em> "
            "is set.",
        ),
        parameters.String(
            name="lonlatbox",
            default=None,
            help="Set the extend of a rectangular lonlatbox "
            "(left_lon, right_lon, lower_lat, upper_lat)",
        ),
        parameters.String(
            name="output_unit",
            mandatory=False,
            default=None,
            help="Set the output unit of the variable - leave blank for no "
            "conversion. This can be useful if the unit of the input "
            "files should be converted, for example for precipitation. "
            "Note: Although many conversions are supported, by using the "
            "`pint` conversion library.",
        ),
        parameters.Float(
            name="vmin",
            default=None,
            help="Set the minimum plotting range (leave blank to calculate "
            "from data 1st decile)",
        ),
        parameters.Float(
            name="vmax",
            default=None,
            help="Set the maximum plotting range (leave blank to calculate "
            "the 9th decile)",
        ),
        parameters.String(
            name="cmap",
            default="RdYlBu_r",
            help="Set the colormap, more information on colormaps is available "
            "on the "
            '<a href="https://matplotlib.org/stable/tutorials'
            '/colors/colormaps.html"'
            " target=_blank>matplotlib website</a>.",
        ),
        parameters.String(
            name="linecolor", default="k", help="Color of the coast lines in the map"
        ),
        parameters.String(
            name="projection",
            default="PlateCarree",
            help="Set the global map projection. Note: this should the name of "
            "the cartopy projection method (e.g PlatteCarree for "
            "Cylindrical Projection). Pleas refer to "
            '<a href="https://scitools.org.uk/cartopy/docs/latest/crs/'
            'projections.html"'
            "target=_blank>cartopy website</a> for details.",
        ),
        parameters.Integer(
            name="proj_centre",
            default="50",
            help="Set center longitude of the global map projection.",
        ),
        parameters.String(
            name="pic_size",
            default="1360,900",
            help="Set the size of the picture (in pixel)",
        ),
        parameters.String(name="plot_title", default="", help="Set plot title"),
        parameters.String(
            name="cbar_label",
            default="",
            help="Overwrite default colorbar label by this value",
        ),
        parameters.SelectField(
            name="suffix",
            default="mp4",
            options={"gif": "gif", "mp4": "mp4"},
            help="Filetype of the animation",
        ),
        parameters.Integer(
            name="fps",
            default=5,
            help="Set the frames per seceonds of the output animation.",
        ),
    )

    def run_tool(self, config_dict=None):
        args = {}
        search_facets = (
            "project",
            "product",
            "experiment",
            "institute",
            "model",
            "time_frequency",
            "ensemble",
            "variable",
        )
        out_dir = self._special_variables.substitute({"d": "$USER_OUTPUT_DIR"})
        out_dir = Path(out_dir["d"]) / str(self.rowid)
        config_dict["output_directory"] = str(out_dir.absolute())
        non_opt = ("variable", "output_directory", "input_file")
        pic_size = config_dict["pic_size"].replace(":", ",").replace(";", ",").strip()
        pic_size = [
            int(f.strip()) for f in config_dict["pic_size"].split(",") if f.strip()
        ]
        config_dict["pic_size"] = pic_size
        try:
            lonlatbox = (
                config_dict["lonlatbox"].replace(";", ",").replace(":", ",").strip()
            )
            lonlatbox = [float(f.strip()) for f in lonlatbox.split(",") if f.strip()]
            config_dict["lonlatbox"] = lonlatbox
        except AttributeError:
            pass
        if config_dict["input_file"]:
            files = config_dict["input_file"]
            if isinstance(files, (str, Path)):
                files = [str(files)]
        else:
            solr_search = {k: config_dict[k] for k in search_facets if config_dict[k]}
            files = list(DRSFile.solr_search(path_only=True, **solr_search))
        for key, value in config_dict.items():
            if key not in search_facets and key not in non_opt and value is not None:
                args[key] = value
        try:
            files.sort()
        except AttributeError:
            files = [files]
        tool_path = Path(__file__).parent / "run.py"
        args["files"] = files
        args["variable"] = config_dict["variable"]
        args["outdir"] = config_dict["output_directory"]
        Path(args["outdir"]).mkdir(exist_ok=True, parents=True)
        for ctype in ("vmin", "vmax"):
            if args.get(ctype, None) is not None:
                args[ctype] = float(args[ctype])
        with NamedTemporaryFile(suffix=".json") as tf:
            with open(tf.name, "w") as f:
                json.dump(args, f)
            this_dir = Path(__file__).parent.absolute()
            python_env = this_dir / "plugin_env" / "bin" / "python"
            cmd = "{} -B {} {}".format(python_env, tool_path, tf.name)
            self.call(cmd)
        return self.prepare_output(config_dict["output_directory"])
